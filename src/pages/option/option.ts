import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController, ModalController } from 'ionic-angular';
import { AuthService } from '../../services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ModalInscription } from '../modal-inscription/modal-inscription';

@Component({
  selector: 'page-option',
  templateUrl: 'option.html',
})
export class OptionPage implements OnInit{

  authForm: FormGroup;
  isAuth: boolean;
  isAuthSub: Subscription;
  errorMessage: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthService, private formBuilder: FormBuilder, public toastCtrl: ToastController, public modalCtrl:ModalController) {
  }

  ngOnInit(){
    this.initForm();
    this.isAuthSub = this.auth.isAuth$.subscribe(
      (isAuth) => {
        this.isAuth = isAuth;
      }
    );
  }

  onSignIn(){
    const email = this.authForm.get('email').value;
    const password = this.authForm.get('password').value;
    this.auth.signInUser(email, password).then(
      () => {
        this.navCtrl.parent.select(1);
        this.toastCtrl.create({
          message: 'Connexion réussie',
          duration: 3000,
          position: 'top'
        }).present();
      },
      (error) => {
        this.toastCtrl.create({
          message: error,
          duration: 3000,
          position: 'top'
        }).present();
      }
    );
  }

  onSignOut(){
    this.auth.signOut();
    this.navCtrl.parent.select(0);
    this.toastCtrl.create({
      message: 'Déconnexion réussie',
      duration: 3000,
      position: 'top'
    }).present();
    this.isAuth = false;
  }

  initForm(){
    this.authForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  onSignUp(){
    const modal = this.modalCtrl.create(ModalInscription);
    modal.present();
  }

}
