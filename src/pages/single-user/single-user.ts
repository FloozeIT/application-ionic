import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { AuthService } from '../../services/auth.service';
import { Subscription } from 'rxjs/Subscription';
import { Geolocation } from '@ionic-native/geolocation';
import { Users } from '../../services/users.service';
import { User } from '../../models/User';

@Component({
  selector: 'single-user',
  templateUrl: 'single-user.html',
})
export class SingleUserPage implements OnInit{
  ngOnInit() {

  }

  mailUser : string;
  mailUserSub: Subscription;
  userList: User[];
  userListSub: Subscription;
  currentUser:User;
  latitude: number;
  longitude: number;
  marker: {
    latitude: number,
    longitude: number,
    draggable: true
  };

  constructor(public toastCtrl:ToastController, public usersService:Users, public geolocation:Geolocation, public loadingCtrl:LoadingController, public navCtrl: NavController, public navParams: NavParams, public auth: AuthService, public userListService: Users) {
    this.getData();
    this.latitude = 57.28;
    this.longitude = -2.58;
    this.onLocateMe();
  }

  getData(){
    this.mailUserSub = this.auth.mailAuthUser$.subscribe(
      (mailUser) => {
        this.mailUser = mailUser;
        if(this.mailUser!=undefined && this.userList!=undefined){
          this.currentUser = this.userList.find(x => x.mail == this.mailUser);
        }
      }
    )
    this.userListSub = this.userListService.userList$.subscribe(
      (userList:User[]) => {
        this.userList = userList;
        if(this.mailUser!=undefined && this.userList!=undefined){
          this.currentUser = this.userList.find(x => x.mail == this.mailUser);
        }
      }
    )
    this.auth.emitAuth();
    this.userListService.emitUserList();
    console.log("=============="+this.mailUser+"=========="+this.userList)
    if(this.mailUser!=undefined && this.userList!=undefined){
      this.currentUser = this.userList.find(x => x.mail == this.mailUser);
    }
  }

  onLocateMe() {
    let loader = this.loadingCtrl.create({
      content: 'Recherche de votre position…'
    });
    loader.present();
    this.geolocation.getCurrentPosition().then(
      (resp) => {
        this.latitude = resp.coords.latitude;
        this.longitude = resp.coords.longitude;
        this.marker = {
          latitude: resp.coords.latitude,
          longitude: resp.coords.longitude,
          draggable: true
        }
        if(this.mailUser!=undefined){
          this.usersService.modifyLocationCurrentUser(this.mailUser,this.longitude,this.latitude);
          loader.dismiss();
        }else{
          loader.dismiss();
        }
      }).catch(
      (error) => {
        loader.dismiss();
        this.toastCtrl.create({
          message: error,
          duration: 3000,
          position: 'bottom'
        }).present();
      }
    );
  }

}
