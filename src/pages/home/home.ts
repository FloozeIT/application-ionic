import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ListUsers } from '../listUsers/listUsers';
import { SingleUserPage } from '../single-user/single-user';
import { OptionPage } from '../option/option';
import firebase from 'firebase';
import { AuthService } from '../../services/auth.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{
  
  ngOnInit() {
    this.isAuthSub = this.auth.isAuth$.subscribe(
      (isAuth) => {
        this.isAuth = isAuth;
      }
    );
  }

  listUsers : any;
  singleUser : any;
  options : any;
  isAuth: boolean;
  isAuthSub: Subscription;

  constructor(public navCtrl: NavController, public auth:AuthService) {
    this.listUsers = ListUsers;
    this.singleUser = SingleUserPage;
    this.options = OptionPage;
  }

  changeAuth(){
    this.isAuth = !this.isAuth;
  }

}
