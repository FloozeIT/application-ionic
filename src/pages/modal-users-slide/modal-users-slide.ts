import { Component, ViewChild, OnInit } from '@angular/core';
import { NavController, NavParams, ViewController, Slides, LoadingController, ToastController  } from 'ionic-angular';
import { Users } from '../../services/users.service';
import { User } from '../../models/User';
import { Geolocation } from '@ionic-native/geolocation';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'page-modal-users-slide',
  templateUrl: 'modal-users-slide.html',
})
export class ModalUsersSlidePage implements OnInit{

  latitude: number;
  longitude: number;
  marker: {
    latitude: number,
    longitude: number,
    draggable: true
  };
  mailUser : string;
  mailUserSub: Subscription;

  ngOnInit(){
    this.latitude = 57.28;
    this.longitude = -2.58;
    this.mailUserSub = this.auth.mailAuthUser$.subscribe(
      (mailUser) => {
        this.mailUser = mailUser;
      }
    )
    this.auth.emitAuth();
    //this.onLocateMe();
  }

  positionSelectUser : number;
  userList :  User[];
  @ViewChild(Slides) slides: Slides;

  constructor(public auth:AuthService, public navCtrl: NavController, public usersService:Users, public navParams: NavParams, private geolocation: Geolocation, public viewCtrl: ViewController, public loadingCtrl:LoadingController, public toastCtrl: ToastController) {
    this.positionSelectUser = this.navParams.get('position');
    this.userList= this.navParams.get('userList');
  }

  public dismiss(){
    this.viewCtrl.dismiss();
  }

  onLocateMe() {
    let loader = this.loadingCtrl.create({
      content: 'Recherche de votre position…'
    });
    loader.present();
    this.geolocation.getCurrentPosition().then(
      (resp) => {
        this.latitude = resp.coords.latitude;
        this.longitude = resp.coords.longitude;
        this.marker = {
          latitude: resp.coords.latitude,
          longitude: resp.coords.longitude,
          draggable: true
        }
        if(this.mailUser!=undefined){
          this.usersService.modifyLocationCurrentUser(this.mailUser,this.longitude,this.latitude);
          loader.dismiss();
        }else{
          loader.dismiss();
        }
      }).catch(
      (error) => {
        loader.dismiss();
        this.toastCtrl.create({
          message: error,
          duration: 3000,
          position: 'bottom'
        }).present();
      }
    );
  }

  /*onMapClicked($event) {
    this.marker = {
      latitude: $event.coords.lat,
      longitude: $event.coords.lng,
      draggable: true
    };
  }*/

  onBombed(){
    
  }

}
