import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, ToastController, LoadingController } from 'ionic-angular';
import { SingleUserPage } from '../single-user/single-user';
import { ModalUsersSlidePage } from '../modal-users-slide/modal-users-slide';
import { Users } from '../../services/users.service';
import { User } from '../../models/User';
import { Subscription } from 'rxjs/Subscription';
import firebase from 'firebase';

@Component({
  selector: 'list-users',
  templateUrl: 'listUsers.html'
})

export class ListUsers implements OnInit{

  userListSubscription : Subscription;
  userList: User[];
  userListFilter: User[];
  showUsers: User[];
  usersConnect : boolean;
  searchTerm: string;

  constructor(public navCtrl : NavController, public modalCtrl: ModalController, public usersService: Users, private toastCtrl: ToastController, private loadingCtrl: LoadingController){
    this.onFetchList();
    this.searchTerm = '';
  }

  ngOnInit(){
    this.userListSubscription = this.usersService.userList$.subscribe(
      (userList: User[]) => {
        this.userList = userList.slice();
        this.userListFilter = this.userList.slice();
      }
    );
    this.usersService.emitUserList();
    this.usersConnect = false;
  }

  getData(){
    this.userListSubscription = this.usersService.userList$.subscribe(
      (userList: User[]) => {
        this.userList = userList.slice();
      }
    );
    this.usersService.emitUserList();
    this.userListFilter = this.userList.slice();
    this.usersConnect = false;
  }

  public onSingleUser(position : number){
    const modal = this.modalCtrl.create(ModalUsersSlidePage, {position:position, userList:this.userListFilter});
    modal.present();
  }

  public onToggleChanged(){
    this.userListFilter = [];
    this.searchTerm = '';
    if(this.usersConnect){
      for (let index = 0; index < this.userList.length; index++) {
        if(this.userList[index].connected){
          this.userListFilter.push(this.userList[index]);
        }        
      }
    }else{
      this.userListFilter = this.userList.slice();
    }
  }


  search(){
    let long = this.searchTerm.length;
    if(long>0){
      this.userListFilter = this.userListFilter.filter((user)=>{
        return user.name.substring(0,long).toLowerCase() == this.searchTerm.toLowerCase();
      });
    }else{
      this.onToggleChanged();
    }
  }

  onFetchList() {
    let loader = this.loadingCtrl.create({
      content: 'Récuperation en cours…'
    });
    loader.present();
    this.usersService.retrieveData().then(
      () => {
        loader.dismiss();
        this.getData();
        this.toastCtrl.create({
          message: 'Données récupérées !',
          duration: 3000,
          position: 'top'
        }).present();
      },
      (error) => {
        loader.dismiss();
        this.toastCtrl.create({
          message: error,
          duration: 3000,
          position: 'top'
        }).present();
      }
    );
  }

  onSaveList() {
    let loader = this.loadingCtrl.create({
      content: 'Sauvegarde en cours…'
    });
    loader.present();
    this.usersService.saveData().then(
      () => {
        loader.dismiss();
        this.toastCtrl.create({
          message: 'Données sauvegardées !',
          duration: 3000,
          position: 'bottom'
        }).present();
      },
      (error) => {
        loader.dismiss();
        this.toastCtrl.create({
          message: error,
          duration: 3000,
          position: 'bottom'
        }).present();
      }
    );
  }

}