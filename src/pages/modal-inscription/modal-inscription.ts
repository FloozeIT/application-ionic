import { Component, ViewChild, OnInit } from '@angular/core';
import { NavController, NavParams, ViewController, Slides, ToastController  } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Users } from '../../services/users.service';
import { User } from '../../models/User';

@Component({
  selector: 'page-modal-inscription',
  templateUrl: 'modal-inscription.html',
})

export class ModalInscription implements OnInit{

  authForm: FormGroup;
  errorMessage:string;

  ngOnInit(){
  }

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController,
    private formBuilder: FormBuilder, 
    public auth:AuthService, 
    public toastCtrl:ToastController,
    public usersService: Users) {
    this.initForm();
  }

  public dismiss(){
    this.viewCtrl.dismiss();
  }

  onSignUp(){
    const email = this.authForm.get('email').value;
    const password = this.authForm.get('password').value;
    const nom = this.authForm.get('nom').value;
    const prenom = this.authForm.get('prenom').value;
    let user = new User((prenom+' '+nom),email);
    this.auth.signUpUser(email, password).then(
      () => {
        this.dismiss()
        this.usersService.addCurrentUser(user);
        this.toastCtrl.create({
          message: 'Inscription réussie',
          duration: 3000,
          position: 'top'
        }).present();
      },
      (error) => {
        this.toastCtrl.create({
          message: error,
          duration: 3000,
          position: 'top'
        }).present();
      }
    );
  }

  initForm(){
    this.authForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      nom: ['', Validators.required],
      prenom: ['', Validators.required]
    });
  }

}
