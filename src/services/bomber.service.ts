import { User } from '../models/User';
import { Subject } from 'rxjs/Subject';
import * as firebase from 'firebase';
import { Injectable, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import { Subscription } from 'rxjs';

@Injectable()

export class Bomber implements OnInit{

    mailUser : string;
    mailUserSub: Subscription;

    constructor(public auth:AuthService){
      
    }

    ngOnInit(){

        this.mailUserSub = this.auth.mailAuthUser$.subscribe(
            (mailUser) => {
                this.mailUser = mailUser;
            }
        )

        firebase.database().ref("Users").on('value', eventSnapchot => {
        
        });
    }

    userList$ = new Subject<User[]>();
    userList : User[]= [];
    
    

  }