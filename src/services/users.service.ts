import { User } from '../models/User';
import { Subject } from 'rxjs/Subject';
import * as firebase from 'firebase';
import { Injectable } from '@angular/core';

@Injectable()

export class Users {

    constructor(){
      firebase.database().ref("Users").on('value', eventSnapchot => {
        this.retrieveData();
      });
    }

    userList$ = new Subject<User[]>();
    userList : User[]= [];
    
    /*userList : User[] = [
      {
        name: 'Florian JANELA',
        mail: 'janelaflorian@gmail.com',
        score: 15,
        connected : true,
        latitude: 57,
        longitude:15
      },
      {
        name: 'Corentin CHATAIGNON',
        mail: 'chataigoncorentin@gmail.com',
        score: 5,
        connected : false,
        latitude: 57,
        longitude:15
      },
      {
        name: 'Alexis DELACROIX',
        mail: 'delacroixalexis@gmail.com',
        score: 10,
        connected : true,
        latitude: 57,
        longitude:15
      },
    ];*/

    emitUserList() {
      this.userList$.next(this.userList.slice());
    }

    saveData() {
      return new Promise((resolve, reject) => {
        firebase.database().ref('Users').set(this.userList).then(
          (data) => {
            resolve(data);
          },
          (error) => {
            reject(error);
          }
        );
      });
    }
  
    retrieveData() {
      return new Promise((resolve, reject) => {
        firebase.database().ref('Users').once('value').then(
          (data) => {
            this.userList = data.val();
            this.emitUserList();
            resolve('Données récupérées avec succès !');
          }, (error) => {
            reject(error);
          }
        );
      });
    }

    modifyStatusCurrentUser(email:string, status:boolean){
      let user = this.userList.find(x => x.mail == email);
      user.connected = status;
      this.saveData().then(
        () => {
          this.retrieveData();
        },
        (error) => {
          console.log(error);
        }
      );
      this.emitUserList();
    }

    modifyLocationCurrentUser(email:string, longitude:number, latitude:number){
      let user = this.userList.find(x => x.mail == email);
      user.longitude = longitude;
      user.latitude = latitude;
      this.saveData().then(
        () => {
          this.retrieveData();
        },
        (error) => {
          console.log(error);
        }
      );
      this.emitUserList();
    }

    addCurrentUser(user:User){
      this.userList.push(user);
      this.saveData().then(
        () => {
          this.retrieveData();
        },
        (error) => {
          console.log(error);
        }
      );
      this.emitUserList();
    }

  }