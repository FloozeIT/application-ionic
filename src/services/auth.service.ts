import * as firebase from 'firebase';
import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { Users } from './users.service';

@Injectable()

export class AuthService {
  
  isAuth$ = new Subject<boolean>();
  isAuth = false;
  mailAuthUser$ = new Subject<string>();
  mailAuthUser:string = '';
  lastMail:string;

  constructor(public userService: Users) {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.isAuth = true;
      } else {
        this.isAuth = false;
      }
    });
  }

  emitAuth() {
    this.isAuth$.next(this.isAuth);
    this.mailAuthUser$.next(this.mailAuthUser);
  }

  signUpUser(email: string, password: string) {
    return new Promise((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(email, password).then(
        (user) => {
          resolve(user);
          this.isAuth=true;
          this.mailAuthUser=email;
          this.lastMail=email;
          this.userService.modifyStatusCurrentUser(email,true);
          this.emitAuth();
        },
        (error) => {
          reject(error);
        }
      );
    });
  }

  signInUser(email: string, password: string) {
    return new Promise((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(email, password).then(
        (user) => {
          resolve(user);
          this.isAuth=true;
          this.mailAuthUser=email;
          this.lastMail=email;
          this.userService.modifyStatusCurrentUser(email,true);
          this.emitAuth();
        },
        (error) => {
          reject(error);
        }
      );
    });
  }

  signOut() {
    firebase.auth().signOut().then(
      ()=>{
        this.isAuth = false;
        this.emitAuth();
        this.userService.modifyStatusCurrentUser(this.lastMail,false);
        this.lastMail="";
      },
      (error)=>{
        console.log(error);
      }
    );
  }
}