export class User {
    name: string;
    mail: string;
    score: number;
    connected : boolean
    latitude: number;
    longitude: number;
  
    constructor(name: string, mail: string) {
        this.mail=mail;
        this.name=name;
        this.score= 0;
        this.connected = true;
        this.latitude = 57;
        this.longitude = 15;
    }
  }