import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListUsers } from '../pages/listUsers/listUsers';
import { SingleUserPage } from '../pages/single-user/single-user';
import { ModalUsersSlidePage } from '../pages/modal-users-slide/modal-users-slide';
import { Users } from '../services/users.service';
import { OptionPage } from '../pages/option/option';
import { AuthService } from '../services/auth.service';
import { ModalInscription } from '../pages/modal-inscription/modal-inscription';
import { Bomber } from '../services/bomber.service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListUsers,
    SingleUserPage,
    ModalUsersSlidePage,
    OptionPage,
    ModalInscription
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot({apiKey: 'AIzaSyDvQURw7t8QSgiPp-GMaALHe1EhxjvHjRo'})
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListUsers,
    SingleUserPage,
    ModalUsersSlidePage,
    OptionPage,
    ModalInscription
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Users,
    AuthService,
    Bomber,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
