import { Component, OnInit } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import firebase from 'firebase';
@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit{
  ngOnInit(): void {
    let config = {
      apiKey: "AIzaSyDDj4WHiyujVeEaT997mEHGp7vLTSCXblI",
      authDomain: "project-test-32819.firebaseapp.com",
      databaseURL: "https://project-test-32819.firebaseio.com",
      projectId: "project-test-32819",
      storageBucket: "project-test-32819.appspot.com",
      messagingSenderId: "529080132602"
    };
    firebase.initializeApp(config);
  }
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

